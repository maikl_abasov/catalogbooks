# CatalogBooks

Тестовое приложение "Каталог книг" 

symfony 5.4.16

php 7.4 

## Установка и запуск

приложение собрано в docker

порт для mysql - 3294  

порт для phpmyadmin - 8283

логин/пароль для базы root:root

вход в контейнер: make bash

```
  make up
  composer install
```

для загрузка тестовых данных
```
  make bash
  php bin/console doctrine:migrations:migrate
  php bin/console doctrine:fixtures:load
```

возможно потребуется задать права для папки var: sudo chmod -R 777 /var

## Rounting

http://localhost:8189/

```
  GET     /author/list
  POST    /author/create
  GET     /author/item/{id}
  POST    /author/update/{id}
  GET     /author/delete/{id}
  GET     /book/list
  POST    /book/create
  GET     /book/item/{id}
  POST    /book/update/{id}
  GET     /book/delete/{id}
```

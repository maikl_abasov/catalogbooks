<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{

    private $authors = [
        [
            'name' => 'АЛЕКСАНДР',
            'surname' => 'ПУШКИН',
            'middlename' => 'СЕРГЕЕВИЧ',
            'date' => '1799-1837',
        ],
        [
            'name' => 'ЛЕВ',
            'surname' => 'ТОЛСТОЙ',
            'middlename' => 'НИКОЛАЕВИЧ',
            'date' => '1828-1910'
        ],
        [
            'name' => 'НИКОЛАЙ',
            'surname' => 'ГОГОЛЬ',
            'middlename' => 'ВАСИЛЬЕВИЧ',
            'date' => '1799-1837'
        ],
        [
            'name' => 'ФЕДОР',
            'surname' => 'ДОСТОЕВСКИЙ',
            'middlename' => 'МИХАЙЛОВИЧ',
            'date' => '1821-1881'
        ],
        [
            'name' => 'АНТОН',
            'surname' => 'ЧЕХОВ',
            'middlename' => 'ПАВЛОВИЧ',
            'date' => '1860-1904'
        ],

        [
            'name' => 'Дэвид',
            'surname' => 'Томас',
            'middlename' => '',
            'date' => ''
        ],

        [
            'name' => 'Эндрю',
            'surname' => 'Хант',
            'middlename' => '',
            'date' => ''
        ],
    ];

    private $books = [
        [
            'title' => 'Дубровский',
            'year' => '2021',
            'isbn' => '978-5-08-006406-7',
            'list_count' => 144,
            'icon_url' => '23456fggggggggggggcontedfggnt.jpg',

            'reference' => 'ПУШКИН'
        ],
        [
            'title' => 'Вишневый сад',
            'year' => '2011',
            'isbn' => '9783-8626-734-14',
            'list_count' => 96,
            'icon_url' => 'cover1__w820-fgh.jpg',

            'reference' => 'ЧЕХОВ',
        ],
        [
            'title' => 'Мертвые души',
            'year' => '2014',
            'isbn' => '9789-8626-734-10',
            'list_count' => 295,
            'icon_url' => '96d56d7d6686febaa99bd0658d3a0eb7.jpg',

            'reference' => 'ГОГОЛЬ',
        ],

        [
            'title' => 'Анна Каренина',
            'year' => '1994',
            'isbn' => '9789-8626-734-77',
            'list_count' => 346,
            'icon_url' => '7-1-639f8711c89ae.jpg',

            'reference' => 'ТОЛСТОЙ',
        ],

        [
            'title' => 'Война и мир',
            'year' => '2012',
            'isbn' => '9789-8626-734-789',
            'list_count' => 2250,
            'icon_url' => 'conerrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrtent.jpg',

            'reference' => 'ТОЛСТОЙ',
        ],

        [
            'title' => 'Братья Карамазовы',
            'year' => '2020',
            'isbn' => '9789-8626-7894-89',
            'list_count' => 1200,
            'icon_url' => 'a6d50e17-c422-4c07-b73d-3b9e722fa1bb.jpg',

            'reference' => 'ДОСТОЕВСКИЙ',
        ],

        [
            'title' => 'Бесы',
            'year' => '2016',
            'isbn' => '9789-8626-7894-678',
            'list_count' => 567,
            'icon_url' => 'fd3e394c-0108-4d21-82f3-13c570cb2a5d.jpg',

            'reference' => 'ДОСТОЕВСКИЙ',
        ],

        [
            'title' => 'Программист-прагматик',
            'year' => '2019',
            'isbn' => '9789-8626-7894-76',
            'list_count' => 800,
            'icon_url' => '10581111082021_accf102caaa970ce65d217b9ae9a8e9a57caa67c.jpg',

            'reference' => 'Томас,Хант',
        ],
    ];

    public function load(ObjectManager $manager): void
    {

        foreach($this->authors as $key => $data) {

            $author = new Author();
            $author->setName($data['name']);
            $author->setSurname($data['surname']);
            $author->setMiddlename($data['middlename']);
            $this->addReference($data['surname'], $author);
            $manager->persist($author);
        }

        $manager->flush();

        foreach ($this->books as $key => $data) {

            $book = new Book();
            $book->setTitle($data['title']);
            $book->setYear($data['year']);
            $book->setIsbn($data['isbn']);
            $book->setListCount($data['list_count']);
            $book->setIconUrl($data['icon_url']);

            if(!empty($data['reference'])) {
                $refList = explode(',', $data['reference']);
                foreach ($refList as  $ref) {
                    $author = $this->getReference($ref);
                    $book->addAuthor($author);
                }
            }

            $manager->persist($book);
        }

        $manager->flush();
    }

}
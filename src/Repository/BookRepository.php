<?php

namespace App\Repository;

use App\Entity\Author;
use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Book>
 *
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    public function delete($id) {
        $book = $this->find($id);
        $iconUrl = $book->getIconUrl();
        $this->getEntityManager()->remove($book);
        $this->getEntityManager()->flush();
        return $iconUrl;
    }

    protected function authorsBind($book, $bookData) {
        if((!empty($bookData['authors_id']))) {
            $authors = $this->getEntityManager()
                ->getRepository(Author::class)
                ->findByInIds($bookData['authors_id']);
            foreach ($authors as $author) {
                $book->addAuthor($author);
            }
        }
        return $book;
    }

    public function create($bookData){

        if($this->checkRow($bookData)) {
            return [
                'status'  => false,
                'message' => 'Такая книга уже существует в каталоге',
            ];
        }

        $book = new Book();
        $book = $this->setData($book, $bookData);
        $book = $this->authorsBind($book, $bookData);
        $this->getEntityManager()->persist($book);
        $this->getEntityManager()->flush();

        return [
            'status'  => true,
            'message' => 'Новая книга добавлена',
            'book'    => $book,
            'book_id' => $book->getId()
        ];
    }

    public function update($id, $bookData)
    {
        if($this->checkRow($bookData, $id)) {
            return [
                'status'  => false,
                'message' => 'Такая книга уже существует в каталоге',
            ];
        }

        $book = $this->find($id);
        $book = $this->setData($book, $bookData);
        $book = $this->authorsBind($book, $bookData);
        $this->getEntityManager()->persist($book);
        $this->getEntityManager()->flush();

        return [
            'status'  => true,
            'message' => 'Информация о книги изменена',
            'book'    => $book,
            'book_id' => $book->getId()
        ];
    }

    protected function setData($book, $data) {
        $book->setTitle($data['title']);
        $book->setYear($data['year']);
        $book->setIsbn($data['isbn']);
        $book->setListCount($data['list_count']);
        $book->setIconUrl($data['icon_url']);
        return $book;
    }

    public function checkRow($data, $id = 0) {

        $params = ['title' => $data['title'], 'isbn' => $data['isbn']];
        $book = $this->findOneBy($params);
        if(!empty($book)) {
            if(!$id) return true;
            if($book->getId() != $id) return true;
        }

        $params = ['title' => $data['title'], 'year' => $data['year']];
        $book = $this->findOneBy($params);
        if(!empty($book)) {
            if(!$id) return true;
            if($book->getId() != $id) return true;
        }

        return false;
    }

    public function findByInIds($booksId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('book')
            ->from(Book::class, 'book')
            ->where("book.id IN(:books_id)")
            ->setParameter('books_id', $booksId)
            ->getQuery()->getResult();
    }

}

<?php

namespace App\Repository;

use App\Entity\Author;
use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Author>
 *
 * @method Author|null find($id, $lockMode = null, $lockVersion = null)
 * @method Author|null findOneBy(array $criteria, array $orderBy = null)
 * @method Author[]    findAll()
 * @method Author[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Author::class);
    }

    public function delete($id)
    {
         $author = $this->find($id);
         $this->getEntityManager()->remove($author);
         return $this->getEntityManager()->flush();
    }

    protected function booksBind($author, $authorData) {
        if((!empty($authorData['books_id']))) {
            $books = $this->getEntityManager()
                ->getRepository(Book::class)
                ->findByInIds($authorData['books_id']);
            foreach ($books as $book) {
                $author->addBook($book);
            }
        }
        return $author;
    }

    public function create(array $authorData) : array
    {
        if($this->checkRow($authorData)) {
            return [
                'status'  => false,
                'message' => 'Такой автор уже существует',
            ];
        }

        $author = new Author();
        $author = $this->setData($author, $authorData);
        $author = $this->booksBind($author, $authorData);
        $this->getEntityManager()->persist($author);
        $this->getEntityManager()->flush();

        return [
            'status'  => true,
            'message' => 'Новый автор добавлен',
            'author'    => $author
        ];
    }

    public function update($id, $authorData)
    {
        if($this->checkRow($authorData, $id)) {
            return [
                'status'  => false,
                'message' => 'Такой автор уже существует',
            ];
        }

        $author = $this->find($id);
        $author = $this->setData($author, $authorData);
        $author = $this->booksBind($author, $authorData);
        $this->getEntityManager()->persist($author);
        $this->getEntityManager()->flush();

        return [
            'status'  => true,
            'message' => 'Новый автор добавлен',
            'author'    => $author
        ];
    }

    protected function setData($author, $data) {
        $author->setName($data['name']);
        $author->setSurname($data['surname']);
        $author->setMiddlename($data['middlename']);
        return $author;
    }

    public function checkRow($data, $id = 0) {
        $params = ['name' => $data['name'], 'surname' => $data['surname']];
        $author = $this->findOneBy($params);
        if(!empty($author)) {
            if(!$id) return true;
            if($author->getId() != $id) return true;
        }
        return false;
    }

    public function findByInIds($authorsId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('author')
            ->from(Author::class, 'author')
            ->where("author.id IN(:authors_id)")
            ->setParameter('authors_id', $authorsId)
            ->getQuery()->getResult();
    }
}

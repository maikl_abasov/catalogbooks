<?php

namespace App\Controller;

use App\Entity\Author;
use App\Entity\Book;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BookController extends AbstractController
{
    protected $manager;

    public function __construct(ManagerRegistry $manager)
    {
       $this->manager = $manager;
    }

    #[Route('/book/list', name: 'book_list')]
    public function getBooks(): Response
    {
        $books = $this->repository(Book::class)->findAll();
        $authors = $this->repository(Author::class)->findAll();

        return $this->render('book/index.html.twig', [
            'title'    => 'Список книг',
            'books'     => $books,
            'authors'  => $authors,
        ]);
    }

    #[Route('/book/create', name: 'book_create')]
    public function create(Request $request, ValidatorInterface $validator)
    {
        $bookData = $request->request->all();
        $isValid = $this->isValidData($validator, $bookData);
        if(!$isValid['status']) {
            return $this->json($isValid);
        }

        $file = $request->files->get('icon_url');
        $bookData['icon_url'] = $this->saveFile($file);
        $response = $this->repository(Book::class)
                         ->create($bookData);
        return $this->json($response);
    }

    #[Route('/book/item/{id}', name: 'book_item')]
    public function getBook($id): Response
    {
        $book = $this->repository(Book::class)->find($id);
        $authors = $this->repository(Author::class)->findAll();

        return $this->render('book/item.html.twig', [
            'title'  => 'Книга',
            'book'   => $book,
            'authors'  => $authors
        ]);
    }

    #[Route('/book/update/{id}', name: 'book_update')]
    public function update($id, Request $request, ValidatorInterface $validator): Response
    {
        $bookData = $request->request->all();
        $isValid = $this->isValidData($validator, $bookData);
        if(!$isValid['status']) {
            return $this->json($isValid);
        }

        $hiddenIconUrl = (!empty($bookData['hidden_icon_url'])) ? $bookData['hidden_icon_url'] : '';
        $bookData['icon_url'] = $hiddenIconUrl;
        $file = $request->files->get('icon_url');
        if(!empty($file)) {
            $this->deleteFile($hiddenIconUrl);
            $bookData['icon_url'] = $this->saveFile($file);
        }
        $response = $this->repository(Book::class)
                         ->update($id, $bookData);
        return $this->json($response);
    }

    #[Route('/book/delete/{id}', name: 'book_delete')]
    public function delete($id, Filesystem $filesystem): Response
    {
        $iconUrl = $this->repository(Book::class)->delete($id);
        $this->deleteFile($iconUrl);
        return $this->redirectToRoute('book_list');
    }

    protected function saveFile($file = null) {
        $iconUrl = '';
        if (!empty($file)) {
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $newFilename = $originalFilename .'-' .uniqid(). '.jpg';
            try {
                $path = $this->getParameter('uploads_dir');
                $file->move($path, $newFilename);
                $iconUrl = $newFilename;
            } catch (FileException $e) {
                dump($e->getMessage()); die;
            }
        }
        return $iconUrl;
    }

    protected function deleteFile($iconUrl) {
        $status = false;
        $path = $this->getParameter('uploads_dir');
        $filePath = $path .'/'. $iconUrl;
        if(is_file($filePath) && file_exists($filePath)) {
            $status = unlink($filePath);
        }
        return $status;
    }

    protected function isValidData(ValidatorInterface $validator, $data) {

         $rules = [
             'title' => [new Assert\NotBlank, new Assert\Length(['min' => 2])],
             'year'  => [new Assert\notBlank],
             'isbn'  => [new Assert\notBlank],
             'list_count'  => [new Assert\notBlank],
         ];

         $constraints = new Assert\Collection($rules);

         $dataValidate = $errors = [];
         foreach ($rules as $name => $item) {
             if(isset($data[$name])) {
                 $dataValidate[$name] = $data[$name];
             }
         }

         $violations = $validator->validate($dataValidate, $constraints);

         $status = true;
         if (0 !== count($violations)) {
            $status = false;
            foreach ($violations as $violation) {
                $path = substr($violation->getPropertyPath(), 1, -1);
                $errors[$path] = $violation->getMessage();
            }
         }

         return [
             'status' => $status,
             'errors' => $errors,
         ];
    }

    protected function repository($class) {
        return $this->manager->getRepository($class);
    }
}

<?php

namespace App\Controller;

use App\Entity\Author;
use App\Entity\AuthorBindBook;
use App\Entity\Book;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class AuthorController extends AbstractController
{

    protected $manager;

    public function __construct(ManagerRegistry $manager)
    {
        $this->manager = $manager;
    }

    #[Route('/author/list', name: 'author_list')]
    public function authors(): Response
    {
        $authors = $this->repository(Author::class)->findAll();
        $books = $this->repository(Book::class)->findAll();
        return $this->render('author/index.html.twig', [
            'title' => 'Список авторов',
            'authors'  => $authors,
            'books' => $books
        ]);
    }

    #[Route('/author/item/{id}', name: 'author_item')]
    public function author($id): Response
    {
        $author = $this->repository(Author::class)->find($id);
        $books = $this->repository(Book::class)->findAll();
        return $this->render('author/item.html.twig', [
            'title'  => 'Автор',
            'author' => $author,
            'books'  => $books,
        ]);
    }

    #[Route('/author/create', name: 'author_create')]
    public function create(Request $request, ValidatorInterface $validator): Response
    {
        $authorData = $request->request->all();
        $isValid = $this->isValidData($validator, $authorData);
        if(!$isValid['status']) {
            return $this->json($isValid);
        }

        $response = $this->repository(Author::class)
                         ->create($authorData);
        return $this->json($response);
    }

    #[Route('/author/update/{id}', name: 'author_update')]
    public function update($id, Request $request, ValidatorInterface $validator): Response
    {

        $authorData = $request->request->all();
        $isValid = $this->isValidData($validator, $authorData);
        if(!$isValid['status']) {
            return $this->json($isValid);
        }

        $response = $this->repository(Author::class)
                         ->update($id, $authorData);
        return $this->json($response);
    }

    #[Route('/author/delete/{id}', name: 'author_delete')]
    public function delete($id): Response
    {
        $response = $this->repository(Author::class)->delete($id);
        return $this->redirectToRoute('author_list');
    }

    protected function repository($class = null) {
        // return $this->getDoctrine()->getRepository($class);
        return $this->manager->getRepository($class);
    }

    protected function isValidData(ValidatorInterface $validator, $data) {

        $rules = [
            'name' => [new Assert\NotBlank, new Assert\Length(['min' => 2])],
            'surname'  => [new Assert\notBlank],
        ];

        $constraints = new Assert\Collection($rules);

        $dataValidate = $errors = [];
        foreach ($rules as $name => $item) {
            if(isset($data[$name])) {
                $dataValidate[$name] = $data[$name];
            }
        }

        $violations = $validator->validate($dataValidate, $constraints);

        $status = true;
        if (0 !== count($violations)) {
            $status = false;
            foreach ($violations as $violation) {
                $path = substr($violation->getPropertyPath(), 1, -1);
                $errors[$path] = $violation->getMessage();
            }
        }

        return [
            'status' => $status,
            'errors' => $errors,
        ];
    }
}

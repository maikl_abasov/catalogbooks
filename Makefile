up:
	docker-compose up -d

down:
	docker-compose down

ps:
	docker-compose ps

bash:
	docker-compose exec php-fpm bash

bash2:
	docker-compose exec oci8 bash

log:
	docker compose logs --tail app

chown:
	sudo chown -R 1000:1000 *





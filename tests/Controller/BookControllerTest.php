<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BookControllerTest extends WebTestCase
{

    protected $bookTitle = 'BookTest1';
    protected $dumpStatus = false;

    protected function getJsonResponse($client) {
        return json_decode($client->getResponse()->getContent(), true);
    }

    protected function printDump($data) {
        if($this->dumpStatus) dump($data);
    }

    // Список книг
    public function testBookList(): void
    {
        $client = static::createClient();
        $client->request('GET', '/book/list');

        $this->assertResponseIsSuccessful(); // Валидировать успешный ответ
        $this->assertSelectorTextContains('h5', 'Список книг'); // содержание
    }

    // Тестируем валидацию
    public function testCreateBookNotValid(): void
    {
        $client = static::createClient();

        $bookData = [
            'title' => '',
            'year' => '',
            'isbn' => '',
            'list_count' => '',
            'authors_id' => [0 => '73', 1 => '74',],
        ];

        $client->request('POST', '/book/create', $bookData);
        $response = $this->getJsonResponse($client);

        $this->assertResponseIsSuccessful(); // Валидировать успешный ответ
        $this->assertSame(false, $response['status'], 'Нет обязательных данных');
        $this->assertSame(true, !empty($response['errors']), 'Нет массива ошибок');

        $this->printDump($response);
    }

    // Успешное создание книги
    public function testCreateBookOk(): void
    {
        $client = static::createClient();

        $response = $this->createBook($client);
        $this->assertResponseIsSuccessful(); // Валидировать успешный ответ
        $this->assertSame(true, $response['status'],  'Статус не подтвержден');
        $this->assertIsInt($response['book_id'], 'Нет Id книги (book_id)');

        $this->deleteBook($response, $client);

        $this->printDump($response);;
    }

    // Проверяем уникальность книги
    public function testCreateBookUniqu(): void
    {
        $client = static::createClient();
        $resp1 = $this->createBook($client); // Создаем книгу

        $resp2 = $this->createBook($client); // Создаем эту же книгу еще раз
        $this->assertSame(false, $resp2['status'],  'Статус не подтвержден');
        $this->assertSame("Такая книга уже существует в каталоге", $resp2['message'],  'Книга добавлена, ошибка');
        $this->deleteBook($resp1, $client);

        $this->printDump($resp1);;
    }

    // 1 книга
    public function testBookItem(): void
    {
        $client = static::createClient();
        $response = $this->createBook($client);

        $client->request('GET', '/book/item/' . $response['book_id']);
        $this->assertResponseIsSuccessful(); // Валидировать успешный ответ
        $this->assertSelectorTextContains('h5', 'Книга: ' . $this->bookTitle); // содержание

        $this->deleteBook($response, $client);
        $this->printDump($response);;
    }


    // Редактирование
    public function testUpdateBook(): void
    {
        $client = static::createClient();
        $response = $this->createBook($client);
        $bookId = $response['book_id'];

        $bookData = [
            'title' => 'BookTest2',
            'year' => '1992',
            'isbn' => '978-5-0444',
            'list_count' => '112',
            // 'hidden_icon_url' => '',
        ];

        $client->request('POST', '/book/update/' . $bookId, $bookData);
        $this->assertResponseIsSuccessful(); // Валидировать успешный ответ
        $response = $this->getJsonResponse($client);
        $this->deleteBook($response, $client);

        $this->assertSame(true, $response['status'],  'Статус не подтвержден');
        $this->assertIsInt($response['book_id'], 'Нет Id книги (book_id)');
        // $this->assertSame("Информация о книги изменена", $response['message']);

        $this->printDump($response);;
    }

    protected function createBook($client) {
        $bookData = [
            'title' => $this->bookTitle,
            'year' => '1992',
            'isbn' => '978-5-0444',
            'list_count' => '112',
            'authors_id' => [0 => '73', 1 => '74',],
        ];
        $client->request('POST', '/book/create', $bookData);
        return $this->getJsonResponse($client);
    }

    protected function deleteBook($response, $client) {
        $client->request('GET', '/book/delete/' . $response['book_id']);
    }
}

var bookPhoto = null;

$(document).ready(function () {

    $('#icon_url').on('change', function(){
        bookPhoto = this.files[0];
    });

});

function pageReload(hash = '', time = 800) {
    setTimeout(function(){
        location.reload();
    }, time);
}

function apiClient(url, data = null, method = 'post') {
    const headers = {
        'content-type': 'multipart/form-data'
    }
    if(data) return axios[method](url, data, { headers });
    return axios[method](url);
}

function formatPostData(formId) {
    const inputs = $(formId).serializeArray();
    let data = new FormData();
    if(bookPhoto) {
        data.append('icon_url', bookPhoto)
    }
    for(let i in inputs) {
        let item = inputs[i];
        data.append(item.name, item.value)
    }
    return data;
}


function createBook(event, form) {
    event.preventDefault();
    bookPhoto = null;
    const data = formatPostData(form)
    apiClient('/book/create', data, 'post')
      .then((response) => {

         let result = response.data;
         let status = bookResponseHandle(result)
         if(!status) return false;
         bookPhoto = null;
         $('#bookModalFormContainer').modal('hide');
         pageReload(100);

      }).catch((error) => {
          console.log('ОШИБКА AJAX ЗАПРОСА');
          console.log(error);
      })
}

function updateBook(event, form, bookId) {
    event.preventDefault();
    bookPhoto = null;
    const data = formatPostData(form)
    apiClient('/book/update/' + bookId, data, 'post')
        .then((response) => {

            let result = response.data;
            let status = bookResponseHandle(result)
            if(!status) return false;
            bookPhoto = null;
            $('#bookModalFormContainer').modal('hide');
            pageReload(100);

        }).catch((error) => {
            console.log('ОШИБКА AJAX ЗАПРОСА');
            console.log(error);
        })
}

function bookResponseHandle(response) {

    $('.validate-error-box').html('');
    $('.model-response-message').html('');
    $('.response-success-message').html('');

    if(!response.status) {
        if(response.errors) {
            for(let name in response.errors) {
                let message = response.errors[name];
                let div = '<div class="validate-error-box">' +message+ '</div>';
                $('input#' + name).after(div);
            }
        }

        if(response.message) {
            $('.model-response-message').html(response.message);
        }

        return false;
    }

    let message = 'Новая книга добавлена';
    if(response.message) message = response.message
    $('.response-success-message').html(message);

    return true;
}

function createAuthor(event, form) {
    event.preventDefault();
    const data = formatPostData(form)
    apiClient('/author/create', data, 'post')
        .then((response) => {

            let result = response.data;
            let status = authorResponseHandle(result)
            if(!status)
                return false;
            $('#authorModalFormContainer').modal('hide');
            pageReload(100);

        }).catch((error) => {
            console.log('ОШИБКА AJAX ЗАПРОСА');
            console.log(error);
        })
}

function updateAuthor(event, form, authorId) {
    event.preventDefault();
    const data = formatPostData(form)
    apiClient('/author/update/' + authorId, data, 'post')
        .then((response) => {

            let result = response.data;
            let status = authorResponseHandle(result)
            if(!status)
                return false;
            $('#authorModalFormContainer').modal('hide');
            pageReload(100);

        }).catch((error) => {
            console.log('ОШИБКА AJAX ЗАПРОСА');
            console.log(error);
        })
}

function authorResponseHandle(response) {

    $('.validate-error-box').html('');
    $('.model-response-message').html('');
    $('.response-success-message').html('');

    if(!response.status) {
        if(response.errors) {
            for(let name in response.errors) {
                let message = response.errors[name];
                let div = '<div class="validate-error-box">' +message+ '</div>';
                $('input#' + name).after(div);
            }
        }

        if(response.message) {
            $('.model-response-message').html(response.message);
        }

        return false;
    }

    let message = 'Новый автор добавлен';
    if(response.message) message = response.message
    $('.response-success-message').html(message);

    return true;
}

// function sendAjax(url, data, callback, method = 'POST') {
//     $.ajax({
//         url  : url,
//         type : method,
//         data : data,
//         processData : false,
//         // dataType : 'json',
//         success : function(response, status, jqXHR) { // функция успешного ответа сервера
//             console.log(response);
//             callback(response);
//         },
//         error: function( jqXHR, status, errorThrown ){ // функция ошибки ответа сервера
//             console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
//         }
//     });
// }




